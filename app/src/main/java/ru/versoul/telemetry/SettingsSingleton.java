package ru.versoul.telemetry;

import android.util.Log;

public class SettingsSingleton {
    private static final String LOG_TAG = "myLogsSettingsSingleton";
    private static SettingsSingleton mInstance;

    public String mac;
    public Integer laps;



    private SettingsSingleton() {
    }
    public static void initInstance() {
        Log.d(LOG_TAG, "MySingleton::InitInstance()");
        if (mInstance == null) {
            mInstance = new SettingsSingleton();
        }
    }
    public static SettingsSingleton getInstance() {
        Log.d(LOG_TAG, "MySingleton::getInstance()");
        return mInstance;
    }

}