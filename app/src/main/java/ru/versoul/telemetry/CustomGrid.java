package ru.versoul.telemetry;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomGrid extends BaseAdapter{
    private Context mContext;
    private ToolsSingleton tools = ToolsSingleton.getInstance();
    private final ArrayList<Racer> racers;


    private String def_null = "― —//― —";

    public CustomGrid(Context c, ArrayList<Racer> racers) {
        mContext = c;
        this.racers = racers;

    }
    @Override
    public int getCount() {
        return racers.size();
    }
    @Override
    public Object getItem(int position) {
        return racers.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        Racer cur_racer = racers.get(position);
        int show_timer = 1;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.grid_single, null);

        } else {
            grid = (View) convertView;
        }
        grid.setId(cur_racer.id);

        TextView txt_name = (TextView) grid.findViewById(R.id.name);
        if(cur_racer.curLap != -1){
            txt_name.setText("["+cur_racer.curLap+"]"+cur_racer.name);
        }
        else{
            txt_name.setText(cur_racer.name);
        }


        int max_race = 0;
        if(cur_racer.race_1 != 0){
            max_race = 1;
            if(cur_racer.race_2 != 0){
                max_race = 2;
                if(cur_racer.race_3 != 0){
                    max_race = 3;
                }
            }
        }


        TextView txt_time_1 = (TextView)grid.findViewById(R.id.time1);
        if(max_race == 1 && cur_racer.raceType == 2 && cur_racer.deltaTimeLocal != 0){
            txt_time_1.setText(tools.millisToString(cur_racer.deltaTimeLocal));
            show_timer = 0;
        }
        else if(cur_racer.race_1 != 0){
            txt_time_1.setText(tools.millisToString(cur_racer.race_1));
        }
        else if(cur_racer.race_1 == 0 && cur_racer.deltaTimeLocal != 0 && show_timer == 1){
            txt_time_1.setText(tools.millisToString(cur_racer.deltaTimeLocal));
            show_timer = 0;
        }
        else{
            txt_time_1.setText(def_null);
        }

        TextView txt_time_2 = (TextView)grid.findViewById(R.id.time2);
        if(max_race == 2 && cur_racer.raceType == 2 && cur_racer.deltaTimeLocal != 0){
            txt_time_2.setText(tools.millisToString(cur_racer.deltaTimeLocal));
            show_timer = 0;
        }
        else if(cur_racer.race_2 != 0){
            txt_time_2.setText(tools.millisToString(cur_racer.race_2));
        }
        else if(cur_racer.race_2 == 0 && cur_racer.deltaTimeLocal != 0 && show_timer == 1){
            txt_time_2.setText(tools.millisToString(cur_racer.deltaTimeLocal));
            show_timer = 0;
        }
        else{
            txt_time_2.setText(def_null);
        }

        TextView txt_time_3 = (TextView)grid.findViewById(R.id.time3);
        if(max_race == 3 && cur_racer.raceType == 2 && cur_racer.deltaTimeLocal != 0){
            txt_time_3.setText(tools.millisToString(cur_racer.deltaTimeLocal));
            show_timer = 0;
        }
        else if(cur_racer.race_3 != 0){
            txt_time_3.setText(tools.millisToString(cur_racer.race_3));
        }
        else if(cur_racer.race_3 == 0 && cur_racer.deltaTimeLocal != 0 && show_timer == 1){
            txt_time_3.setText(tools.millisToString(cur_racer.deltaTimeLocal));
            show_timer = 0;
        }
        else{
            txt_time_3.setText(def_null);
        }

        TextView txt_time_best = (TextView)grid.findViewById(R.id.best_time);
        if(cur_racer.deltaTimeLocal != 0){
            txt_time_best.setText(tools.millisToString(cur_racer.deltaTimeLocal));
        }
        else{
            txt_time_best.setText(def_null);
        }


        return grid;
    }
}