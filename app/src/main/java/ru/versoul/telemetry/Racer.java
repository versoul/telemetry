package ru.versoul.telemetry;

public class Racer {

    public int id;//Его ид в базе
    public String name;
    public long race_1;
    public long race_2;
    public long race_3;
    public int raceType;//Тип рейса, новый - 1 или рестарт - 2
    public int lapsRemained;//Кругов осталось
    public int lapsAll;//Кругов всего
    public int curLap;//Текущий круг
    public long startTime;//Стартовое время с ардуины
    public long startTimeLocal;//Стартовое время с андроида
    public long deltaTimeLocal;//Дельта времени на андроиде


    Racer(int id, String name, long race_1, long race_2, long race_3){
        this.id = id;
        this.name = name;
        this.race_1 = race_1;
        this.race_2 = race_2;
        this.race_3 = race_3;
        this.raceType = 0;
        this.curLap = -1;
        /*this.raceType = raceType;
        this.lapsRemained = lapsRemained;
        this.startTime = startTime;
        this.stopTime = stopTime;*/
    }
}
