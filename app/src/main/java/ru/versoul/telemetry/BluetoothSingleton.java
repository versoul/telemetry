package ru.versoul.telemetry;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;
import android.os.Handler;
import android.widget.Toast;

public class BluetoothSingleton {
    private static final String LOG_TAG = "myLogsBluetooth";
    private static BluetoothSingleton mInstance;
    private ToolsSingleton tools = ToolsSingleton.getInstance();
    private SettingsSingleton settings = SettingsSingleton.getInstance();
    private MainActivity MainActivityLink;


    final int RECIEVE_MESSAGE = 1;        // Статус для Handler
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder sb = new StringBuilder();
    long pingTime = 0;
    long pongTime = 0;
    int bluetoothState = 0;

    private ConnectedThread mConnectedThread;

    // SPP UUID сервиса
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");



    private Handler h = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RECIEVE_MESSAGE:                                                   // если приняли сообщение в Handler
                    byte[] readBuf = (byte[]) msg.obj;
                    String strIncom = new String(readBuf, 0, msg.arg1);
                    sb.append(strIncom);
                    //Log.d(LOG_TAG, "...Строка1:"+sb);
                    int endOfLineIndex = sb.indexOf("&");                            // определяем символы конца строки
                    if (endOfLineIndex > 0) {                                            // если встречаем конец строки,
                        String sbprint = sb.substring(0, endOfLineIndex);               // то извлекаем строку
                        sb.delete(0, sb.length());                                      // и очищаем sb
                        if(sbprint.equals("0")){
                            pongTime = System.currentTimeMillis();
                        }
                        else{
                            //Log.d(LOG_TAG, "...Строка2:"+sbprint);
                            if(tools.arduinoHandler(sbprint)){
                                Log.d(LOG_TAG, "команда распознана");
                                mInstance.write("2");
                            }
                            else{
                                Log.d(LOG_TAG, "команда НЕНЕНЕ распознана");
                            }

                        }
                    }
                    break;
            }
        };
    };


    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();

            } catch (IOException e) { }




            mmInStream = tmpIn;
            mmOutStream = tmpOut;

            //checkConnection();
        }

        public void run() {
            byte[] buffer = new byte[256];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);        // Получаем кол-во байт и само собщение в байтовый массив "buffer"
                    h.obtainMessage(RECIEVE_MESSAGE, bytes, -1, buffer).sendToTarget();     // Отправляем в очередь сообщений Handler
                } catch (IOException e) {
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String message) {
            if(!message.equals("0")){
                Log.d(LOG_TAG, "...Данные для отправки: " + message + "...");
            }

            byte[] msgBuffer = message.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            } catch (IOException e) {
                Log.d(LOG_TAG, "...Ошибка отправки данных: " + e.getMessage() + "...");
                if(e.getMessage().equals("Broken pipe")){
                    Log.d(LOG_TAG, "Исходящий сокет отвалился");
                }
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void close() {
            try {
                mmInStream.close();
                mmOutStream.close();
                mmSocket.close();
            } catch (IOException e) { }
        }
    }
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(LOG_TAG, "Broadcast ="+action);
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        //Toast.makeText(getApplicationContext(), "Bluetooth off", Toast.LENGTH_LONG).show();
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        //Toast.makeText(getApplicationContext(), "Turning Bluetooth off...", Toast.LENGTH_LONG).show();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(LOG_TAG, "Bluetooth Включился");
                        connectToDevice();
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        //Toast.makeText(getApplicationContext(), "Turning Bluetooth on...", Toast.LENGTH_LONG).show();
                        break;
                }
            }
            else if(action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)){
                //checkConnection();
                setBtnBluetoothState("green");
                Log.d(LOG_TAG, "Connected");
                bluetoothState = 1;
                Toast.makeText(MainActivityLink, "Соединение установлено", Toast.LENGTH_LONG).show();
            }
            else if(action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)){
                setBtnBluetoothState("red");
                mConnectedThread.close();
                Log.d(LOG_TAG, "Disconnected");
                bluetoothState = 0;
                Toast.makeText(MainActivityLink, "Соединение разорвано", Toast.LENGTH_LONG).show();
            }

        }
    };
    private void checkState() {
        // Check for Bluetooth support and then check to make sure it is turned on
        // Emulator doesn't support Bluetooth and will return null
        if(btAdapter==null) {
            Log.d(LOG_TAG, "Bluetooth не поддерживается");
            MainActivityLink.setBtnBluetoothState("red");
        } else {
            if (btAdapter.isEnabled()) {
                Log.d(LOG_TAG, "...Bluetooth включен...");
                connectToDevice();
            } else {
                //Prompt user to turn on Bluetooth
                MainActivityLink.requestEnableBluetooth();


                // Register for broadcasts on BluetoothAdapter state change
                IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
                MainActivityLink.registerReceiver(mReceiver, filter);
            }
        }
    }

    public void connectToDevice(){
        setBtnBluetoothState("yellow");

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(settings.mac.equals("")){
                    Log.d(LOG_TAG, "mac адрес не задан");
                    Toast.makeText(MainActivityLink, "Задайте mac адрес устройства в настройках", Toast.LENGTH_LONG).show();
                    setBtnBluetoothState("white");
                }
                else{
                    //Пытаемся проделать эти действия
                    try{

                        BluetoothDevice device = btAdapter.getRemoteDevice(settings.mac);


                        // Register for broadcasts on BluetoothAdapter state change
                        IntentFilter filter1 = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
                        IntentFilter filter2 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
                        IntentFilter filter3 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
                        MainActivityLink.registerReceiver(mReceiver, filter1);
                        MainActivityLink.registerReceiver(mReceiver, filter2);
                        MainActivityLink.registerReceiver(mReceiver, filter3);

                        //Инициируем соединение с устройством
                        Method m = device.getClass().getMethod(
                                "createRfcommSocket", new Class[] {int.class});

                        btSocket = (BluetoothSocket) m.invoke(device, 1);
                        btSocket.connect();


                        mConnectedThread = new ConnectedThread(btSocket);
                        mConnectedThread.setPriority(Thread.MAX_PRIORITY);
                        mConnectedThread.start();

                        //В случае появления любых ошибок, выводим в лог сообщение
                    } catch (IOException e) {
                        String msg = e.getMessage();
                        Log.d(LOG_TAG, "1"+msg);
                        if(msg.equals("Connection refused") || msg.equals("read failed, socket might closed or timeout, read ret: -1")){
                            setBtnBluetoothState("white");
                            Log.d(LOG_TAG, "Не удалось подключиться к устройству");
                            Toast.makeText(MainActivityLink, "Не удалось подключиться к устройству", Toast.LENGTH_LONG).show();
                            setBtnBluetoothState("white");
                        }
                    } catch (SecurityException e) {
                        Log.d(LOG_TAG, "2"+e.getMessage());
                        setBtnBluetoothState("white");
                    } catch (NoSuchMethodException e) {
                        Log.d(LOG_TAG, "3"+e.getMessage());
                        setBtnBluetoothState("white");
                    } catch (IllegalArgumentException e) {
                        Log.d(LOG_TAG, "4"+e.getMessage());
                        setBtnBluetoothState("white");
                    } catch (IllegalAccessException e) {
                        Log.d(LOG_TAG, "5"+e.getMessage());
                        setBtnBluetoothState("white");
                    } catch (InvocationTargetException e) {
                        Log.d(LOG_TAG, "6"+e.getMessage());
                        setBtnBluetoothState("white");
                    }
                }

            }
        }, 1);
    }
    public void connect(){
        btAdapter = BluetoothAdapter.getDefaultAdapter();       // получаем локальный Bluetooth адаптер
        checkState();
    }
    private void setBtnBluetoothState(String state){
        MainActivityLink.setBtnBluetoothState(state);
    }
    public int getState(){
        return bluetoothState;
    }





    private BluetoothSingleton() {
        Log.w(LOG_TAG, "Create singlton");

    }
    public static void initInstance() {
        Log.d(LOG_TAG, "MySingleton::InitInstance()");
        if (mInstance == null) {
            mInstance = new BluetoothSingleton();
        }
    }
    public static BluetoothSingleton getInstance() {
        Log.d(LOG_TAG, "MySingleton::getInstance()");
        return mInstance;
    }
    public void setMainActivity(MainActivity act) {
        MainActivityLink = act;
    }
    public void write(String message) {
        if( bluetoothState == 1){
            mConnectedThread.write(message);
        }
        else{
            Log.d(LOG_TAG, "...Отправка сообщения не возможна, нет подключения...");
        }

    }

}