package ru.versoul.telemetry;

import android.os.Handler;
import android.text.format.DateFormat;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ToolsSingleton {

    private static final String LOG_TAG = "myLogsToolsSingleton";
    private static ToolsSingleton mInstance;
    private MainActivity MainActivityLink;
    private SettingsSingleton settings = SettingsSingleton.getInstance();

    public int active_racerId = 0;//Выбранный в гриде гонщик
    int curRacerNum = 0;//едущий сейчас гонщик
    private boolean reload_loop = false;
    DBHelper dbHelper;

    public ArrayList<Racer> racers = new ArrayList<Racer>();
    public ArrayList<Integer> racersInRace = new ArrayList<Integer>();


    private Handler reload_h = new Handler() {
        public void handleMessage(android.os.Message msg) {
            MainActivityLink.reloadGrid();
            if(racersInRace.size() != 0){
                refreshGrid();
            }
            else{
                //t.stop();
                //reload_loop = false;
            }
        };
    };
    private Thread t = new Thread(new Runnable() {
        private Racer finded_racer = null;
        private Long cur_time;
        public void run() {
            while(true){
                finded_racer = null;
                cur_time = System.currentTimeMillis();
                for(int i=0; i<racersInRace.size(); i++){
                    finded_racer = getRacerById(racersInRace.get(i));
                    if(finded_racer.startTimeLocal != 0){
                        finded_racer.deltaTimeLocal = cur_time - finded_racer.startTimeLocal;
                    }
                }
                try {
                    TimeUnit.MILLISECONDS.sleep(55);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                reload_h.sendEmptyMessage(0);
            }
        }
    });

    private ToolsSingleton() {
    }
    public static void initInstance() {
        Log.d(LOG_TAG, "MySingleton::InitInstance()");
        if (mInstance == null) {
            mInstance = new ToolsSingleton();

        }
    }
    public static ToolsSingleton getInstance() {
        //Log.d(LOG_TAG, "MySingleton::getInstance()");
        return mInstance;
    }
    public void setMainActivity(MainActivity act) {
        MainActivityLink = act;
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(MainActivityLink);

    }
    public boolean arduinoHandler(String data){
        String arduinoCommand = "";
        String arduinoData = "";
        boolean ret = false;
        Pattern p = Pattern.compile("-=(.+)=(.+)=-");
        Matcher m = p.matcher(data);
        while (m.find()) {
            arduinoCommand = m.group(1);
            arduinoData = m.group(2);
            if(arduinoCommand != "" && arduinoData != ""){
                ret = true;
            }
        }
        //Log.d(LOG_TAG, "str-"+data+"cmd1-"+arduinoCommand+"-dta1"+arduinoData);
        if(arduinoCommand.equals("S")){
            //Зеленый сигнал светофора
            //detect_line(Long.parseLong(arduinoData));
        }
        else if(arduinoCommand.equals("L")){
            //Пересечение линии
            detect_line(Long.parseLong(arduinoData));
            //Log.d(LOG_TAG, "detect line in tools");
        }
        return ret;
    }


    public void refreshGrid(){
        if(!reload_loop){
            t.setPriority(Thread.MIN_PRIORITY);
            t.start();
            reload_loop = true;
        }
    }

    public void detect_line(long time){

        Log.d(LOG_TAG, "cur_racer1 = "+curRacerNum+"---"+racersInRace.size());


        //Если в гонке есть участники
        if(racersInRace.size() != 0) {
            Racer cur_racer = null;


            if (curRacerNum < racersInRace.size() ) {
                cur_racer = getRacerById(racersInRace.get(curRacerNum));

                if(cur_racer.startTime == 0){
                    cur_racer.startTime = time;
                    cur_racer.startTimeLocal = System.currentTimeMillis();

                    Log.d(LOG_TAG, "line = Start ");
                    cur_racer.curLap = 1;

                    curRacerNum += 1;
                }
                else{
                    cur_racer = getRacerById(racersInRace.get(curRacerNum));
                    if (cur_racer.lapsRemained != 0) {


                        cur_racer.lapsRemained -= 1;
                        cur_racer.curLap = cur_racer.lapsAll - cur_racer.lapsRemained+1;

                        Log.d(LOG_TAG, "lineL1");

                        if (cur_racer.lapsRemained == 0) {
                            Log.d(LOG_TAG, "line = Finish1");

                            finish(cur_racer, time);

                            racersInRace.remove(curRacerNum);
                        } else {
                            curRacerNum += 1;
                        }
                    }
                }
            }
            else if (curRacerNum == racersInRace.size()) {

                curRacerNum = 0;
                cur_racer = getRacerById(racersInRace.get(curRacerNum));
                if (cur_racer.lapsRemained != 0) {


                    cur_racer.lapsRemained -= 1;
                    cur_racer.curLap = cur_racer.lapsAll - cur_racer.lapsRemained+1;

                    Log.d(LOG_TAG, "lineL2");

                    if (cur_racer.lapsRemained == 0) {
                        Log.d(LOG_TAG, "line = Finish2");

                        finish(cur_racer, time);

                        racersInRace.remove(curRacerNum);
                    }
                    else{

                        curRacerNum += 1;
                    }
                }
            }

        }
        else{
            Log.d(LOG_TAG, "empty else");
        }
    }
    public void finish(Racer cur_racer, Long stopTime){

        //если новый заезд
        if(cur_racer.raceType == 1){
            if(cur_racer.race_1 == 0){
                cur_racer.race_1 = stopTime - cur_racer.startTime;
            }
            else if(cur_racer.race_2 == 0){
                cur_racer.race_2 = stopTime - cur_racer.startTime;
            }
            else if(cur_racer.race_3 == 0){
                cur_racer.race_3 = stopTime - cur_racer.startTime;
            }

        }//Если рестарт
        else if(cur_racer.raceType == 2){
            if(cur_racer.race_1 == 0){
                cur_racer.race_1 = stopTime - cur_racer.startTime;
            }
            else if(cur_racer.race_2 == 0){
                cur_racer.race_1 = stopTime - cur_racer.startTime;
            }
            else if(cur_racer.race_3 == 0){
                cur_racer.race_2 = stopTime - cur_racer.startTime;
            }
            else {
                cur_racer.race_3 = stopTime - cur_racer.startTime;
            }
        }


        dbHelper.update_cur_racer(cur_racer);


        cur_racer.startTime = 0;
        cur_racer.startTimeLocal = 0;
        cur_racer.deltaTimeLocal = 0;
        cur_racer.raceType = 0;
        cur_racer.lapsAll = 0;
        cur_racer.curLap = -1;


        //dbHelper.finish(curRacerNum, time);

        MainActivityLink.reloadGrid();
        Log.d(LOG_TAG, "Finish!!");
    }
    public void stop_race(){
        Racer cur_racer;
        for(int i=0; i<racersInRace.size(); i++){
            cur_racer = getRacerById(racersInRace.get(i));

            cur_racer.startTime = 0;
            cur_racer.startTimeLocal = 0;
            cur_racer.deltaTimeLocal = 0;
            cur_racer.raceType = 0;
            cur_racer.lapsAll = 0;
            cur_racer.curLap = -1;

        }
        racersInRace.clear();
        curRacerNum = 0;
    }
    public void out_of_race(){
        Racer cur_racer;

        cur_racer = getRacerById(active_racerId);

        cur_racer.startTime = 0;
        cur_racer.startTimeLocal = 0;
        cur_racer.deltaTimeLocal = 0;
        cur_racer.raceType = 0;
        cur_racer.lapsAll = 0;
        cur_racer.curLap = -1;

        int pos = racersInRace.indexOf(active_racerId);
        racersInRace.remove(pos);

        if(curRacerNum > racersInRace.size()){
            curRacerNum -= 1;
        }



        active_racerId = 0;
    }
    public String millisToString(Long millis){//TODO вынести в отдельный класс
        String time = String.format("%d:%02d.%03d",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)),
                millis - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(millis))
        );
        return time;
    }
    public Racer getRacerById(int id){
        Racer finded_racer = null;
        for(int i=0; i<racers.size(); i++){
            if(racers.get(i).id == id){
                finded_racer =  racers.get(i);
            }
        }
        return finded_racer;
    }
}