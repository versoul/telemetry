package ru.versoul.telemetry;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;


public class SettingsActivity extends ActionBarActivity {
    private static final String LOG_TAG = "myLogsSettings";
    private SettingsSingleton settings = SettingsSingleton.getInstance();

    DBHelper dbHelper;
    Button btnSave;
    EditText inpMac, inpLaps;
    TextView ver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(this);


        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(btnSaveClick);

        inpMac = (EditText)findViewById(R.id.inpMac);
        inpMac.setText(settings.mac);

        inpLaps = (EditText)findViewById(R.id.inpLaps);
        inpLaps.setText(String.valueOf(settings.laps));

        ver = (TextView)findViewById(R.id.version);
        ver.setText("Версия приложения: "+BuildConfig.VERSION_NAME);

    }

    View.OnClickListener btnSaveClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Editable valMac = inpMac.getText();
            settings.mac = valMac.toString();
            Editable valLaps = inpLaps.getText();
            settings.laps = Integer.valueOf(valLaps.toString());
            dbHelper.saveSettings();

            finish();
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
