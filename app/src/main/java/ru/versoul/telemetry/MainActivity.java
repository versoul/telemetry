package ru.versoul.telemetry;

import ru.versoul.telemetry.util.SystemUiHider;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.view.MenuItem;
import android.view.ContextMenu.ContextMenuInfo;


import android.content.Intent;
import android.bluetooth.BluetoothAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.database.sqlite.SQLiteDatabase;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class MainActivity extends Activity {
    private static final String LOG_TAG = "myLogsMain";
    private BluetoothSingleton bt = BluetoothSingleton.getInstance();
    private ToolsSingleton tools = ToolsSingleton.getInstance();
    private SettingsSingleton settings = SettingsSingleton.getInstance();
    DBHelper dbHelper;
    ImageButton btnBluetooth;
    ImageButton btnMenu;
    GridView gvMain;


    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
    private SystemUiHider mSystemUiHider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Log.d(LOG_TAG, "--- onCreate database ---");


        btnBluetooth = (ImageButton)findViewById(R.id.btnBluetooth);
        btnBluetooth.setOnClickListener(btnBluetoothClick);
        btnMenu = (ImageButton)findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(btnMenuClick);





        if(bt.getState() == 1){
            setBtnBluetoothState("green");
        }
        else{
            setBtnBluetoothState("white");
        }

        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(this);

        // подключаемся к БД
        //SQLiteDatabase db = dbHelper.getWritableDatabase();
        //Устанавливаем ссылку на MainActivity в синглтоне
        bt.setMainActivity(this);
        tools.setMainActivity(this);

        gvMain = (GridView) findViewById(R.id.gvMain);
        gvMain.setOnCreateContextMenuListener(this);


        dbHelper.update_racers();
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) menuInfo;
        tools.active_racerId = info.targetView.getId();

        //Log.d(LOG_TAG, "grid item = " + info.targetView.getId());
        getMenuInflater().inflate(R.menu.grid_menu, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Racer cur_racer = tools.getRacerById(tools.active_racerId);
        switch (item.getItemId()) {
            case R.id.action_sendStart:
                if(cur_racer.race_3 != 0){
                    Toast.makeText(this,
                            "Все попытки исчерпаны!", Toast.LENGTH_LONG).show();
                }
                else if(cur_racer.raceType != 0){
                    Toast.makeText(this,
                            "Уже в заезде!", Toast.LENGTH_LONG).show();
                }
                else{
                    cur_racer.startTime = Long.valueOf(0);
                    cur_racer.raceType = 1;
                    cur_racer.lapsRemained = settings.laps;
                    cur_racer.lapsAll = settings.laps;
                    cur_racer.curLap = 0;
                    tools.racersInRace.add(tools.active_racerId);

                    Log.d(LOG_TAG, "Start - "+ cur_racer.name);

                    bt.write("1");


                    tools.refreshGrid();
                }
                return true;
            case R.id.action_sendRestart:
                if(cur_racer.race_1 == 0 && cur_racer.race_2 == 0 && cur_racer.race_3 == 0){
                    Toast.makeText(this,
                            "Нечего повторять, заездов раньше небыло!", Toast.LENGTH_LONG).show();
                }
                else if(cur_racer.raceType != 0){
                    Toast.makeText(this,
                            "Уже в заезде!", Toast.LENGTH_LONG).show();
                }
                else{
                    cur_racer.startTime = Long.valueOf(0);
                    cur_racer.raceType = 2;
                    cur_racer.lapsRemained = settings.laps;
                    cur_racer.lapsAll = settings.laps;
                    cur_racer.curLap = 0;
                    tools.racersInRace.add(tools.active_racerId);

                    Log.d(LOG_TAG, "Restart - "+ cur_racer.name);

                    bt.write("1");


                    tools.refreshGrid();
                }
                return true;
            case R.id.action_outRace:
                tools.out_of_race();
                return true;
            case R.id.action_clearTime:
                dbHelper.clearRacerTimes(tools.active_racerId);
                reloadGrid();
                return true;
            default:

                tools.active_racerId = 0;
                return super.onContextItemSelected(item);
        }
    }
    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        reloadGrid();
    }

    View.OnClickListener btnBluetoothClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            bt.connect();
        }
    };
    View.OnClickListener btnMenuClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openOptionsMenu();
        }
    };
    public void setBtnBluetoothState(String state){
        if(state == "green"){
            btnBluetooth.setBackgroundResource(R.drawable.btn_bluetooth_green);
        }
        else if(state == "white"){
            btnBluetooth.setBackgroundResource(R.drawable.btn_bluetooth_white);
        }
        else if(state == "red"){
            btnBluetooth.setBackgroundResource(R.drawable.btn_bluetooth_red);
        }
        else if(state == "yellow"){
            btnBluetooth.setBackgroundResource(R.drawable.btn_bluetooth_yellow);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addRacer:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Добавление участника");
                alert.setIcon(android.R.drawable.btn_star_big_on);
                alert.setMessage("Введите имя и класс");



                View linearlayout = getLayoutInflater().inflate(R.layout.alert_add_racer, null);
                alert.setView(linearlayout);

                final EditText input = (EditText)linearlayout.findViewById(R.id.name);


                alert.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Editable value = input.getText();
                        dbHelper.addRacer(value.toString());
                        reloadGrid();
                    }
                });
                /*alert.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Если отменили.
                    }
                });*/
                alert.show();
                return true;
            case R.id.action_stopRace:
                tools.stop_race();
                return true;
            case R.id.action_clearTimes:
                dbHelper.clearRacerTimes(-1);
                reloadGrid();
                return true;
            case R.id.action_clearRacers:
                dbHelper.clearRacers();
                reloadGrid();
                return true;
            case R.id.action_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
            case R.id.action_line1:
                tools.detect_line(1000);
                return true;
            case R.id.action_line2:
                tools.detect_line(3000);
                return true;
            case R.id.action_exit:
                finish();
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    /*----------*-*-*-*-*----------*/



    public void requestEnableBluetooth(){
        //Выводит запрос на включение блютуза
        String enableBT = BluetoothAdapter.ACTION_REQUEST_ENABLE;
        startActivityForResult(new Intent(enableBT), 0);
    }
    public void set_racer_laps(Integer lap){
        ViewGroup gridChild = (ViewGroup) gvMain.findViewById(tools.active_racerId);
        TextView text = (TextView) gridChild.findViewById(R.id.lap);
        if(lap != -1){
            text.setText("["+lap+"]");
        }
        else{
            text.setText("");
        }
    }
    public void reloadGrid(){

        CustomGrid adapter = new CustomGrid(MainActivity.this, tools.racers);
        gvMain = (GridView) findViewById(R.id.gvMain);
        gvMain.setAdapter(adapter);
    }
    /*public void finishh(int cur_racer, Long time){
        dbHelper.finish(cur_racer, time);
    }*/
}
