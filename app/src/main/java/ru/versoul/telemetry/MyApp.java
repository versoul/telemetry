package ru.versoul.telemetry;

import android.app.Application;
import android.content.Context;
import android.util.Log;

public class MyApp extends Application {
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        Log.w("myLogsApp", "onCreate MyApp");

        ToolsSingleton.initInstance();
        SettingsSingleton.initInstance();
        BluetoothSingleton.initInstance();


    }
    public static Context getContext() {
        return sContext;
    }
}
