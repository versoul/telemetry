package ru.versoul.telemetry;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.content.Context;

class DBHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = "myLogsDB";
    private SettingsSingleton settings = SettingsSingleton.getInstance();
    private ToolsSingleton tools = ToolsSingleton.getInstance();



    public void saveSettings (){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("mac", settings.mac);
        cv.put("laps", settings.laps);
        String selection = "id = ?";
        String[] selectionArgs = { "1" };
        int count = db.update(
                "settings",
                cv,
                selection, selectionArgs);
        Log.d(LOG_TAG, "row updatetcount = " + count);
        db.close();
    }
    public void readSettings(){
        Log.d("LOG_TAG", "reading settings ");
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM settings";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String mac = cursor.getString(cursor.getColumnIndex("mac"));
            settings.mac = mac;
            Integer laps = cursor.getInt(cursor.getColumnIndex("laps"));
            settings.laps = laps;
        }
        cursor.close();
        db.close();
    }
    public void addRacer (String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name", name);
        long rowID = db.insert("races", null, cv);
        Log.d(LOG_TAG, "row inserted, ID = " + rowID);
        db.close();
        tools.racers.add(new Racer((int) rowID, name,0, 0, 0));
    }
    public void clearRacers () {
        SQLiteDatabase db = this.getWritableDatabase();
        int clearCount = db.delete("races", null, null);
        db.close();
        update_racers();
        Log.d(LOG_TAG, "deleted rows count = " + clearCount);
    }
    public void clearRacerTimes(int id){
        Log.d(LOG_TAG, "db clear all times");
        SQLiteDatabase db = this.getWritableDatabase();
        if(id != -1){
            db.execSQL("UPDATE races SET race_1='0',race_2='0',race_3='0' WHERE id="+id);
        }
        else{
            db.execSQL("UPDATE races SET race_1='0',race_2='0',race_3='0'");
        }
        db.close();
        update_racers();
    }
    public void update_cur_racer(Racer cur_racer){
        Log.d(LOG_TAG, "db finish ");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put("race_1", cur_racer.race_1);
        cv.put("race_2", cur_racer.race_2);
        cv.put("race_3", cur_racer.race_3);
        String selection = "id = ?";
        String[] selectionArgs = { String.valueOf(cur_racer.id) };
        int count = db.update(
                "races",
                cv,
                selection, selectionArgs);
        //db.execSQL("UPDATE mytable SET race_1='234' WHERE id=1");
        db.close();
    }
    public void update_racers(){
        SQLiteDatabase db = this.getWritableDatabase();

        Log.d(LOG_TAG, "--- Rows in mytable: ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        Cursor c = db.query("races", null, null, null, null, null, null);

        tools.racers.clear();//isEmpty()
        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {
            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG,
                        "ID = " + c.getInt(c.getColumnIndex("id")) +
                                ", name = " + c.getInt(c.getColumnIndex("name")) +
                                ", race_1 = " + c.getInt(c.getColumnIndex("race_1")));


                tools.racers.add(new Racer(c.getInt(c.getColumnIndex("id")),
                                        c.getString(c.getColumnIndex("name")),
                                        c.getLong(c.getColumnIndex("race_1")),
                                        c.getLong(c.getColumnIndex("race_2")),
                                        c.getLong(c.getColumnIndex("race_3"))));
            } while (c.moveToNext());
        }
        else {
            Log.d(LOG_TAG, "0 rows");
        }
        c.close();
        db.close();
    }






    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context, "main.db", null, 1);// версия БД

        readSettings();
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "--- onCreate database ---");
        // создаем таблицу с полями
        db.execSQL("create table settings ("
                + "id integer primary key,"
                + "mac text,"
                + "laps integer NOT NULL" + ");");
        db.execSQL("INSERT INTO settings (id, mac, laps) "
            + "VALUES (1, '00:12:05:16:99:52', 2);");


        db.execSQL("create table races ("
                + "id integer primary key autoincrement,"
                + "name text,"
                + "race_1 integer default 0,"
                + "race_2 integer default 0,"
                + "race_3 integer default 0"
                + ");");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE IF EXISTS settings");
        //onCreate(db);
    }
}